BASE_IMAGE_REPOSITORY=docker.io/python
DOCKER_REGISTRY=registry.gitlab.com
DOCKER_IMAGE_NAME=python
DOCKER_REPOSITORY_BASEPATH=cloudmigration/base-images
PYTHON_VERSIONS=3.5-slim-stretch 3.6-slim-stretch 3.7-slim-stretch 3.8-slim-buster
BASE_IMAGE_VERSIONS=$(PYTHON_VERSIONS)
AORTA_VERSION=1.0.26
GUNICORN_VERSION=20.0.4
