GCC_PKG_NAME=gcc
GPP_PKG_NAME=g++
OS_USER=nobody
OS_GROUP=nogroup
OS_PKG_UPDATE=apt-get update -y
OS_PKG_INSTALL=apt-get install -y
OS_PKG_PURGE=apt-get autoremove --purge -y
PKG_BUILD=pkg-config gcc
