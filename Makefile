include config.mk
ifdef DOCKER_BASE_IMAGE_TAG
include includes/$(DOCKER_BASE_IMAGE_TAG).mk
include includes/$(OS_NAME).mk
include includes/$(OS_NAME).$(OS_RELEASE).mk
endif
DOCKER_REPOSITORY_PATH=$(DOCKER_REPOSITORY_BASEPATH)/$(DOCKER_IMAGE_NAME)
ifdef DOCKER_BASE_IMAGE_TAG
DOCKER_QUALNAME=$(DOCKER_REPOSITORY_PATH):$(DOCKER_BASE_IMAGE_TAG)
endif
DOCKER_PUSH_RULES := $(addprefix docker-push-, $(BASE_IMAGE_VERSIONS))
DOCKER_BUILD_RULES := $(addprefix docker-build-, $(BASE_IMAGE_VERSIONS))


docker-build:
	@make $(DOCKER_BUILD_RULES)


docker-push:
	@make $(DOCKER_PUSH_RULES)


docker-build-%:
	@make _docker-build DOCKER_BASE_IMAGE_TAG=$(*)

docker-push-%:
	@make _docker-build DOCKER_BASE_IMAGE_TAG=$(*)
	@make _docker-push DOCKER_BASE_IMAGE_TAG=$(*)


_docker-build:
	@docker build -t $(DOCKER_REPOSITORY_PATH):$(DOCKER_BASE_IMAGE_TAG) .\
		-f Dockerfile.$(OS_NAME)\
		--build-arg "BASE_IMAGE_REPOSITORY=$(BASE_IMAGE_REPOSITORY)"\
		--build-arg "BASE_IMAGE_TAG=$(DOCKER_BASE_IMAGE_TAG)"\
		--build-arg "OS_PKG_UPDATE=$(OS_PKG_UPDATE)"\
		--build-arg "OS_PKG_INSTALL=$(OS_PKG_INSTALL)"\
		--build-arg "OS_PKG_PURGE=$(OS_PKG_PURGE)"\
		--build-arg "OS_USER=$(OS_USER)"\
		--build-arg "OS_GROUP=$(OS_GROUP)"\
		--build-arg "AORTA_VERSION=$(AORTA_VERSION)"\
		--build-arg "GUNICORN_VERSION=$(GUNICORN_VERSION)"\
		--build-arg "PKG_LIBSSL=$(PKG_LIBSSL)"\
		--build-arg "PKG_SECCOMP=$(PKG_SECCOMP)"\
		--build-arg "PKG_BUILD=$(PKG_BUILD)"


_docker-push:
	@docker tag $(DOCKER_QUALNAME) $(DOCKER_REGISTRY)/$(DOCKER_QUALNAME)
	@docker push $(DOCKER_REGISTRY)/$(DOCKER_QUALNAME)
